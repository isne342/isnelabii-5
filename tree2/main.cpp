//
//  main.cpp
//  tree2
//
//  Created by Panpech Pothong on 9/7/2559 BE.
//  Copyright © 2559 Panpech Pothong. All rights reserved.
//
#include <iostream>
#include <string>
#include "tree.h"
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>

using namespace std;
int main()
{
    int num ;
    
    cout << "How many number  :   ";
    cin >> num;
    
    Tree<int> mytree(num);
    Tree<int> baTree;
    int random_int;
    int search;
    int numberd;
    cout << "Insert Random numbers to  tree : " ;
    srand(time(0));
    for (int i = 0; i < num; i++) {
        random_int = (rand() % 100) + 1;
        cout << random_int << "  ";
        mytree.insert(random_int);
        
    }
    
    cout << endl;
    cout << "height : " << mytree.height() << endl;
    cout << "Inorder : ";
    mytree.inorder();
    cout << endl;
    
    cout << "Balance : ";
    baTree.balance(mytree.arr_tree,0,num-1);
    cout << endl;
    cout << "Height : " << baTree.height() << endl;
    cout << "Search number : ";
    cin >> search;
    if (baTree.search(search)) {
        cout << "Found" << endl;
    }
    else cout << "Not Found"<<endl;
    cout << "How many times to delete  :  " ;
    cin >> numberd;
    cout << endl;
    for (int i = 0; i < numberd; i++) {
        random_int = (rand() % 100) + 1;
        cout << "Delete random #" << i << "  ";
        cout << random_int << "  ";
        if (baTree.search(random_int)) {
            cout << "Deleted" << endl;
            baTree.deletion(random_int);
        }
        else {
            cout << "Not found" << endl;
        }
        cout << endl;
    }
    cout << endl;
    baTree.printinorder();
    cout << endl;
}
