//
//  tree.h
//  tree2
//
//  Created by Panpech Pothong on 9/7/2559 BE.
//  Copyright © 2559 Panpech Pothong. All rights reserved.
//
#define tree_h
#include <queue>
#include <stack>
using namespace std;

#ifndef Binary_Search_Tree
#define Binary_Search_Tree

template<class T> class Tree;

template<class T>
class Node {
public:
    Node() { left = right = NULL; }
    Node(const T& el, Node *l = 0, Node *r = 0) {
        key = el; left = l; right = r;
    }
    T key;
    Node *left, *right;
};

template<class T>
class Tree {
public:
    Tree() { root = 0; }
    Tree(int a) {
        root = 0;
        num = a;
        arr_tree = new int[num];
        index = 0;
    }
    ~Tree() { clear(); }
    void clear() { clear(root); root = 0; }
    bool isEmpty() { return root == 0; }
    void inorder() { inorder(root); }
    void insert(const T& el);
    void deleteNode(Node<T> *& node, Node<T> *&Prevnode);
    int  height() { return height(root); }
    void print();
    void balance(T data[], int first, int last);
    int *arr_tree;
    bool search(const T& S);
    void deletion(int);
    void printinorder() { printinorder(root); }
    
protected:
    Node<T> *root;
    
    void clear(Node<T> *p);
    void inorder(Node<T> *p);
    void printinorder(Node<T> *p);
    int  height(Node<T> *p);
    int num;
    int index;
    
};

template<class T>
void Tree<T>::clear(Node<T> *p)
{
    if (p != 0) {
        clear(p->left);
        clear(p->right);
        delete p;
    }
}

template<class T>
void Tree<T>::inorder(Node<T> *p) {
    
    if (root != NULL) {
        if (p->left != NULL) {
            inorder(p->left);
        }
        cout << p->key << " ";
        
        arr_tree[index] = p->key;
        index++;
        
        if (p->right != NULL) {
            inorder(p->right);
        }

    }
    else {
        cout << " The tree is empty" << endl;
    }
    
}
template<class T>
void Tree<T>::insert(const T &el) {
    Node<T> *p = root, *prev = 0;
    while (p != 0) {
        prev = p;
        if (p->key < el)
            p = p->right;
        else
            p = p->left;
    }
    if (root == 0)
        root = new Node<T>(el);
    else if (prev->key<el)
        prev->right = new Node<T>(el);
    else
        prev->left = new Node<T>(el);
}

template<class T>
void Tree<T>::deleteNode(Node<T> *&node, Node<T> *&Prevnode) {
    Node<T> *prev, *tmp = node;
    
    if (node->right == 0)
    {
        if (Prevnode->left == node) {
            node = node->left;
            Prevnode->left = node;
        }
        else {
            node = node->left;
            Prevnode->right = node;
        }
        
        
    }
    else if (node->left == 0) {
        
        if (Prevnode->left == node) {
            node = node->right;
            Prevnode->left = node;
        }
        else {
            node = node->right;
            Prevnode->right = node;
        }
        
    }
    else {
        tmp = node->left;
        prev = node;
        while (tmp->right != 0) {
            prev = tmp;
            tmp = tmp->right;
        }
        node->key = tmp->key;
        if (prev == node)
            prev->left = tmp->left;
        else prev->right = tmp->left;
    }
    delete tmp;
}

template<class T>
int Tree<T>::height(Node<T> *p) {
    int l_height = 0, r_height = 0;
    if (root != NULL) {
        if (p->left != NULL) {
            
            l_height = height(p->left);
            
        }
        else {
            l_height = 0;
        }
        if(p->right != NULL){
            
            
            r_height = height(p->right);
        }
        else
        {
            r_height = 0;
        }
        return (l_height >= r_height) ? l_height+1 : r_height+1;
    }
    else
    {
        return 0;
    }
    
}
template<class T>
void Tree<T>::print() {
    for (int i = 0; i < num; i++) {
        cout << arr_tree[i] << " ";
        
    }
    cout << endl;
}
template<class T>
void Tree<T>::balance(T data[], int first, int last) {
    if (first <= last) {
        int middle = (first + last) / 2;
        insert(data[middle]);
        cout << data[middle] << " ";
        balance(data, first, middle - 1);
        balance(data, middle + 1, last);
    }
}
template<class T>
bool Tree<T>::search(const T &S) {
    Node<T> *p = root;
    while (p != 0) {
        if (p->key == S) {
            return true;
        }
        else if(p->key > S) {
            p = p->left;
        }
        else{
            p = p->right;
        }
    }
    return false;
}

template<class T>
void Tree<T> ::deletion(int D) {
    Node<T> *p = root;
    Node<T> *tmp = root;
    while (p->key != D) {
        tmp = p;
        if (p->key > D) {
            p = p->left;
        }
        else {
            p = p->right;
        }
    }
    deleteNode(p,tmp);
}

template<class T>
void Tree<T>::printinorder(Node<T> *p) {
    
    if (root != NULL) {
        if (p->left != NULL) {
            printinorder(p->left);
        }
        cout << p->key << " ";
        if (p->right != NULL) {
            printinorder(p->right);
        }
    }
    else {
        cout << " The tree is empty" << endl;
    }
    
}
#endif // Binary_Search_Tree
